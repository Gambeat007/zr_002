import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private Socket socket;
    private PrintWriter out;
    private  BufferedReader in;
    private Scanner scanner = new Scanner(System.in);

    public void startConnection (String ip, int port) throws IOException {
        socket = new Socket(ip, port);
        out = new PrintWriter(socket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        System.out.println("\nEnter specific command or type \"help\" to see available commands");
        String command;
        while ((command = scanner.nextLine()) != null) {
            if (command.equalsIgnoreCase("stop")) {
                stopConnection();
            }
            sendRequest(command);
        }
    }

    private void sendRequest(String request) throws IOException {
        out.println(request);
        getResponse();
    }

    private void getResponse() throws IOException {
        String response = in.readLine();
        System.out.println(response);
    }

    private void stopConnection() throws IOException {
        System.out.println("World has been stopped. See you!");
        socket.close();
        in.close();
        out.close();
        System.exit(0);
    }
}
