import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.Duration;
import java.time.LocalDateTime;

public class Server {
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private BufferedReader in;
    private PrintWriter out;

    private Gson gson;
    private LocalDateTime serverStartTime;
    private ServerInfo serverInfo = new ServerInfo();

    public void start(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        serverStartTime = LocalDateTime.now();
        System.out.println("\nServer is running, start time: "+serverStartTime);

        clientSocket = serverSocket.accept();
        System.out.println("\nClient connected from socket: "+clientSocket);

        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        out = new PrintWriter(clientSocket.getOutputStream(), true);

        getRequest();
    }

    private void getRequest() throws IOException {
        String input;
        while ((input = in.readLine()) != null) {
            System.out.println("Client: "+input);
            switch (input) {
                case "help" -> showHelp();
                case "uptime" -> showUptime();
                case "info" -> showInfo();
                case "stop" -> stop();
                default -> sendResponse("Invalid command. Type \"help\" to see available commands.");
            }
        }
    }

    private void showHelp() {
        sendResponse(convertToJson("help -available commands, " +
                "info -server version and creation date, " +
                "uptime -server uptime, stop -stops client and server"));
    }

    private void showInfo() {
        sendResponse(convertToJson(serverInfo));
    }

    private void showUptime() {
        LocalDateTime now = LocalDateTime.now();
        Duration uptime = Duration.between(serverStartTime, now);
        long uptimeMins = uptime.toMinutes();
        long uptimeSecs = uptime.toSeconds();
        sendResponse(convertToJson("Server uptime: "+uptimeMins % 60+
                " minutes "+uptimeSecs % 60+" seconds "));
    }

    private void stop() throws IOException {
        serverSocket.close();
        clientSocket.close();
        in.close();
        out.close();
        System.exit(0);
    }

    private void sendResponse(String response) {
        out.println(response);
    }

    private String convertToJson(Object object) {
        gson = new Gson();
        return gson.toJson(object);
    }
}

