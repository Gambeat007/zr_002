public class ServerInfo {
    private String version = "3.0.0.2";
    private String creationDate = "11-11-2020";

    public String getVersion() {
        return version;
    }

    public String getCreationDate() {
        return creationDate;
    }
}
